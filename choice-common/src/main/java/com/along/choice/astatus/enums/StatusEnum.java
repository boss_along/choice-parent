package com.along.choice.astatus.enums;


import com.along.choice.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: bosslong
 * @Date: 2019/7/5 14:56
 * @Description:
 */
@AllArgsConstructor
@Getter
public enum StatusEnum {

    SAVE(1, "已暂存"),

    SUBMIT(2,"已提交"),

    CHECK(3,"已审核"),

    CANCLE(5,"已撤销"),

    EXPIRE(6, "已作废"),

    DELETE(7,"已删除"),

    REJECT(8,"已驳回"),

    RETURNCHECK(9,"反审核");



    private Integer code;

    private String desc;


    public static StatusEnum valueOf(Integer code){
        for(StatusEnum statusEnum : StatusEnum.values()){
            if(code.equals(statusEnum.getCode())){
                return statusEnum;
            }
        }
        throw new BusinessException("未知状态");
    }

}
