package com.along.choice.astatus.busines;

import com.along.choice.astatus.BaseStatus;
import com.along.choice.astatus.StatusBeanFactory;


/**
 * @author: bosslong
 * @Date: 2019/7/11 15:36
 * @Description:
 */
public class TestController {

    private static TestService a = new TestServiceImpl();


    public static void main(String[] args) throws Exception {
        BaseStatus baseStatus = StatusBeanFactory.newInstance(2, TestController.a);
        baseStatus.doTempSave(new Stu());
    }
}
