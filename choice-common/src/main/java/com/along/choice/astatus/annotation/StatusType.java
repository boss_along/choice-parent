package com.along.choice.astatus.annotation;

import com.along.choice.astatus.enums.StatusEnum;

import java.lang.annotation.*;

/**
 * @author: bosslong
 * @Date: 2019/7/12 16:38
 * @Description:
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface StatusType {

    StatusEnum value();
}
