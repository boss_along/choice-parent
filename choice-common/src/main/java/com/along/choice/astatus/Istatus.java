package com.along.choice.astatus;

/**
 * @author: bosslong
 * @Date: 2019/7/5 14:43
 * @Description:
 */
public interface Istatus {

    /**
     * 暂存
     * @param obj
     */
    Object doTempSave(Object obj);

    /**
     * 提交
     * @param obj
     */
    Object doSubmit(Object obj);

    /**
     * 审核
     * @param obj
     */
    Object doCheck(Object obj);

    /**
     * 撤回
     * @param obj
     */
    Object doCancle(Object obj);

    /**
     * 驳回
     * @param obj
     */
    Object doReject(Object obj);

    /**
     * 反审核
     * @param obj
     */
    Object doReturnCheck(Object obj);

    /**
     * 作废
     * @param obj
     */
    Object doExpire(Object obj);

    /**
     * 删除
     * @param obj
     */
    Object doDelete(Object obj);

}
