package com.along.choice.astatus.busines;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: bosslong
 * @Date: 2019/7/5 15:24
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Stu {

    private Integer id;

    private Integer statusCode;
}
