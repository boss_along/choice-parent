package com.along.choice.astatus.status;

import com.along.choice.astatus.BaseStatus;
import com.along.choice.astatus.annotation.StatusType;
import com.along.choice.astatus.enums.StatusEnum;

/**
 * @author: bosslong
 * @Date: 2019/7/11 19:49
 * @Description:
 */
@StatusType(StatusEnum.REJECT)
public class StatusReject extends BaseStatus {
    @Override
    public Object doSubmit(Object obj) {
        return super.doSubmit(obj);
    }

    @Override
    public Object doCheck(Object obj) {
        return businessService.doCheck(obj);
    }

    @Override
    public Object doExpire(Object obj) {
        return super.doExpire(obj);
    }


}
