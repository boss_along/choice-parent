package com.along.choice.astatus;

/**
 * @author: bosslong
 * @Date: 2019/7/5 15:00
 * @Description:
 */
public interface BusinessService<T> {

    /**
     * 暂存
     * @param t
     */
    default Object doTempSave(T t){
        return null;
    }

    /**
     * 提交
     * @param t
     */
    default Object doSubmit(T t){
        return null;
    }

    /**
     * 审核
     * @param t
     */
    default Object doCheck(T t){
        return null;
    }

    /**
     * 撤回
     * @param t
     */
    default Object doCancle(T t){
        return null;
    }

    /**
     * 驳回
     * @param t
     */
    default Object doReject(T t){
        return null;
    }

    /**
     * 反审核
     * @param t
     */
    default Object doReturnCheck(T t){
        return null;
    }

    /**
     * 作废
     * @param t
     */
    default Object doExpire(T t){
        return null;
    }

    /**
     * 删除
     * @param t
     */
    default Object doDelete(T t){
        return null;
    }
}
