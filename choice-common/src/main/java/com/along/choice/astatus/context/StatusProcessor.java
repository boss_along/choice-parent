package com.along.choice.astatus.context;

import com.along.choice.astatus.annotation.StatusType;
import com.along.choice.astatus.enums.StatusEnum;
import com.google.common.collect.Maps;
import org.cent.scanner.core.callback.ScannerCallback;
import org.cent.scanner.core.scanner.ClassScanner;
import org.cent.scanner.core.scanner.impl.DefaultClassScanner;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Thinkpad
 * @Title: HandlerProcessor
 * @ProjectName blog
 * @Description: TODO
 * @date 2019/5/2817:00
 * @Version: 1.0
 */
@Component
public class StatusProcessor implements BeanFactoryPostProcessor {

    private static final String SCAN_PACKAGE = "com.along.choice.astatus.status" ;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {

        Map handlerMap = Maps.newHashMapWithExpectedSize(3);

        ClassScanner classScanner = new DefaultClassScanner();

        classScanner.scanAndCallbackByAnno(Arrays.asList(SCAN_PACKAGE), StatusType.class, new ScannerCallback() {
            @Override
            public void callback(List<Class> list) {
                list.forEach(clazz -> {
                    StatusType annotation = (StatusType)clazz.getAnnotation(StatusType.class);
                    StatusEnum value = annotation.value();
                    if(Objects.isNull(value)){
                        throw new IllegalArgumentException("HandlerType value expected not null but null");
                    }
                    handlerMap.put(value,clazz);
                });
            }
        });

        StatusContext context = new StatusContext(handlerMap);
        configurableListableBeanFactory.registerResolvableDependency(StatusContext.class,context);
    }
}
