package com.along.choice.astatus.status;

import com.along.choice.astatus.BaseStatus;
import com.along.choice.astatus.annotation.StatusType;
import com.along.choice.astatus.enums.StatusEnum;

/**
 * @author: bosslong
 * @Date: 2019/7/11 19:48
 * @Description:
 */
@StatusType(StatusEnum.CANCLE)
public class StatusCancel extends BaseStatus {
    @Override
    public Object doTempSave(Object obj) {
        return businessService.doTempSave(obj);
    }

    @Override
    public Object doSubmit(Object obj) {
        return businessService.doSubmit(obj);
    }

    @Override
    public Object doCheck(Object obj) {
        return businessService.doCheck(obj);
    }
}
