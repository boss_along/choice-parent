package com.along.choice.astatus.status;

import com.along.choice.astatus.BaseStatus;
import com.along.choice.astatus.annotation.StatusType;
import com.along.choice.astatus.enums.StatusEnum;

/**
 * @author: bosslong
 * @Date: 2019/7/11 19:50
 * @Description:
 */
@StatusType(StatusEnum.RETURNCHECK)
public class StatusReturnCheck extends BaseStatus {


    @Override
    public Object doSubmit(Object obj) {
        return businessService.doSubmit(obj);
    }

    @Override
    public Object doCheck(Object obj) {
        return businessService.doCheck(obj);
    }

    @Override
    public Object doExpire(Object obj) {
        return businessService.doExpire(obj);
    }
}
