package com.along.choice.astatus.status;

import com.along.choice.astatus.BaseStatus;
import com.along.choice.astatus.annotation.StatusType;
import com.along.choice.astatus.enums.StatusEnum;

/**
 * @author: bosslong
 * @Date: 2019/7/11 19:49
 * @Description:
 */
@StatusType(StatusEnum.DELETE)
public class StatusDelete extends BaseStatus {
}
