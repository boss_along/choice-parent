package com.along.choice.astatus;


import com.along.choice.exception.BusinessException;

/**
 * @author: bosslong
 * @Date: 2019/7/5 14:44
 * @Description:
 */
public abstract class BaseStatus implements Istatus {

    protected BusinessService businessService;

    @Override
    public Object doTempSave(Object obj) {
        throw new BusinessException("当前状态不可暂存");
    }

    @Override
    public Object doSubmit(Object obj) {
        throw new BusinessException("当前状态不可提交");
    }

    @Override
    public Object doCheck(Object obj) {
        throw new BusinessException("当前状态不可审核");
    }

    @Override
    public Object doCancle(Object obj) {
        throw new BusinessException("当前状态不可撤销");
    }

    @Override
    public Object doReject(Object obj) {
        throw new BusinessException("当前状态不可驳回");
    }

    @Override
    public Object doReturnCheck(Object obj) {
        throw new BusinessException("当前状态不可反审核");
    }

    @Override
    public Object doExpire(Object obj) {
        throw new BusinessException("当前状态不可作废");
    }

    @Override
    public Object doDelete(Object obj) {
        throw new BusinessException("当前状态不可删除");
    }
}
