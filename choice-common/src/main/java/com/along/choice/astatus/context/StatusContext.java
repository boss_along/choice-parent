package com.along.choice.astatus.context;

import com.along.choice.astatus.enums.StatusEnum;
import java.util.Map;

/**
 * @author Thinkpad
 * @Title: HandlerContext
 * @ProjectName blog
 * @Description: TODO
 * @date 2019/5/3019:42
 * @Version: 1.0
 */
public class StatusContext {

    private static Map<StatusEnum,Class> handlerMap;

    public StatusContext(Map<StatusEnum, Class> handlerMap) {
        StatusContext.handlerMap = handlerMap;
    }

    public static Class getStatusTypeClass(StatusEnum type) {
        return handlerMap.get(type);
    }
}
