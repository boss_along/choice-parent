package com.along.choice.astatus.status;

import com.along.choice.astatus.BaseStatus;
import com.along.choice.astatus.annotation.StatusType;
import com.along.choice.astatus.enums.StatusEnum;

/**
 * @author: bosslong
 * @Date: 2019/7/5 15:11
 * @Description:
 */
@StatusType(StatusEnum.CHECK)
public class StatusCheck  extends BaseStatus {

    @Override
    public Object doReturnCheck(Object obj) {
        return businessService.doReturnCheck(obj);
    }
}
