package com.along.choice.astatus;

import com.along.choice.astatus.context.StatusContext;
import com.along.choice.astatus.enums.StatusEnum;

import java.lang.reflect.Field;

/**
 * @author: bosslong
 * @Date: 2019/7/11 17:50
 * @Description:
 */
public class StatusBeanFactory {


    public static BaseStatus newInstance(Integer statusCode, BusinessService businessService) throws Exception{

        StatusEnum statusEnum = StatusEnum.valueOf(statusCode);

        Class statusClazz = StatusContext.getStatusTypeClass(statusEnum);

        Class<?> clazz = Class.forName(statusClazz.getName());

        return invoke((BaseStatus)clazz.newInstance(),businessService);

    }

    private static BaseStatus invoke(BaseStatus baseStatus,BusinessService businessService) throws Exception{
        Field businessServiceFiled = BaseStatus.class.getDeclaredField("businessService");
        businessServiceFiled.setAccessible(true);
        businessServiceFiled.set(baseStatus,businessService);
        return baseStatus;
    }
}
