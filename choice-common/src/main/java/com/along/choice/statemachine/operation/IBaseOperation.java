package com.along.choice.statemachine.operation;

import com.along.choice.statemachine.request.BaseRequest;
import com.along.choice.statemachine.state.StateEnum;

public interface IBaseOperation<REQ extends BaseRequest,RESP> {

    boolean support(StateEnum state);

    RESP doOperate(REQ req);
}
