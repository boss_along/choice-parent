package com.along.choice.statemachine.operation;

import com.along.choice.statemachine.request.TestRequest;
import com.along.choice.statemachine.state.StateEnum;

public class Operationupdate extends AbstractOperation<TestRequest,Object> {


    @Override
    public Object businessOperation(TestRequest testRequest) {
        System.out.println("更新");
        return null;
    }

    @Override
    public void throwBusinessException() {
        throw new RuntimeException("当前状态不可更新");
    }
}
