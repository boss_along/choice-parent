package com.along.choice.statemachine.operation;

import com.along.choice.statemachine.request.BaseRequest;
import com.along.choice.statemachine.state.StateEnum;

import java.util.Iterator;
import java.util.Set;

public abstract class AbstractOperation<REQ extends BaseRequest ,RESP> implements IBaseOperation<REQ,RESP> {

    protected Set<StateEnum> states;

    @Override
    public boolean support(StateEnum state) {
        Iterator<StateEnum> iterator = states.iterator();
        while(iterator.hasNext()){
            StateEnum next = iterator.next();
            if(next.equals(state)){
                return true;
            }
        }
        return false;
    }

    @Override
    public RESP doOperate(REQ req) {
        StateEnum state = req.getState();
        if(!support(state)){
            throwBusinessException();
        }
        return businessOperation(req);
    }

    public abstract RESP businessOperation(REQ req);

    public abstract void throwBusinessException();
}
