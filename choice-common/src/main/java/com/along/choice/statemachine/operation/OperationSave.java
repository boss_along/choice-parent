package com.along.choice.statemachine.operation;

import com.along.choice.statemachine.request.TestRequest;

public class OperationSave extends AbstractOperation<TestRequest,Object> {

    @Override
    public Object businessOperation(TestRequest testRequest) {
        System.out.println("暂存");
        return null;
    }


    @Override
    public void throwBusinessException() {
        throw new RuntimeException("当前状态不能暂存");
    }
}
