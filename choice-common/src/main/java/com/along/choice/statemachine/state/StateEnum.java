package com.along.choice.statemachine.state;

public enum StateEnum {

    SAVE,
    DELETE,
    UPDATE,
    QUERY
}
