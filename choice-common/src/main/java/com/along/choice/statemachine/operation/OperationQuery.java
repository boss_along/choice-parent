package com.along.choice.statemachine.operation;

import com.along.choice.statemachine.request.TestRequest;

public class OperationQuery extends AbstractOperation<TestRequest,Object> {

    @Override
    public Object businessOperation(TestRequest testRequest) {
        System.out.println("查询");
        return null;
    }

    @Override
    public void throwBusinessException() {
        throw new RuntimeException("当前状态不能查询");
    }
}
