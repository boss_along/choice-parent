package com.along.choice.statemachine.request;

import com.along.choice.statemachine.state.StateEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseRequest {

    private StateEnum state;
}
