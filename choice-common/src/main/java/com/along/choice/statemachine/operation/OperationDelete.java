package com.along.choice.statemachine.operation;

import com.along.choice.statemachine.request.TestRequest;

public class OperationDelete extends AbstractOperation<TestRequest,Object> {

    @Override
    public Object businessOperation(TestRequest testRequest) {
        System.out.println("删除");
        return null;
    }

    @Override
    public void throwBusinessException() {
        throw new RuntimeException("当前状态不能删除");
    }
}
