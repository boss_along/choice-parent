package com.along.choice.statemachine;

import com.along.choice.statemachine.operation.OperationDelete;
import com.along.choice.statemachine.operation.OperationQuery;
import com.along.choice.statemachine.operation.OperationSave;
import com.along.choice.statemachine.operation.Operationupdate;

public interface IBaseStateMachine<REQ> {

    Object save(REQ req, OperationSave operationSave);

    Object delete(REQ req, OperationDelete operationDelete);

    Object update(REQ req, Operationupdate operationupdate);

    Object query(REQ req, OperationQuery operationQuery);
}
