package com.along.choice.exception;

/**
 * @author: bosslong
 * @Date: 2019/7/4 17:38
 * @Description: 业务异常类
 */
public class BusinessException extends RuntimeException {

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

}
