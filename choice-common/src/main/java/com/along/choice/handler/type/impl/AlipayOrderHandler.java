package com.along.choice.handler.type.impl;

import com.along.choice.handler.annotation.HandlerType;
import com.along.choice.handler.constant.HandlerTypeEnum;
import com.along.choice.handler.type.OrderBaseHandler;

/**
 * @author Thinkpad
 * @Title: AlipayOrderHandler
 * @ProjectName blog
 * @Description: TODO
 * @date 2019/6/1819:57
 * @Version: 1.0
 */
@HandlerType(HandlerTypeEnum.ALIPAY_ORDER)
public class AlipayOrderHandler extends OrderBaseHandler<Object,Object> {

    @Override
    public Object execute(Object param) {
        System.out.println("支付宝订单");
        return null;
    }
}
