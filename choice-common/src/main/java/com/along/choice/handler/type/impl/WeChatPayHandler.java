package com.along.choice.handler.type.impl;

import com.along.choice.handler.HandlerContext;
import com.along.choice.handler.annotation.HandlerType;
import com.along.choice.handler.constant.HandlerTypeEnum;
import com.along.choice.handler.type.PayBaseHandler;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Thinkpad
 * @Title: WeChatPayHandler
 * @ProjectName blog
 * @Description: TODO
 * @date 2019/6/1819:28
 * @Version: 1.0
 */
@HandlerType(HandlerTypeEnum.WECHAT_PAY)
public class WeChatPayHandler extends PayBaseHandler<Object,Object>  {

    @Override
    public Object execute(Object param) {
        System.out.println("微信支付");
        return null;
    }
}
