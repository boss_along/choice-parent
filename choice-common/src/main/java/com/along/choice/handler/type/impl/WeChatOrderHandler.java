package com.along.choice.handler.type.impl;

import com.along.choice.handler.HandlerContext;
import com.along.choice.handler.annotation.HandlerType;
import com.along.choice.handler.constant.HandlerTypeEnum;
import com.along.choice.handler.type.OrderBaseHandler;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Thinkpad
 * @Title: WeChatOrderHandler
 * @ProjectName blog
 * @Description: TODO
 * @date 2019/6/199:24
 * @Version: 1.0
 */
@HandlerType(HandlerTypeEnum.WECHAT_ORDER)
public class WeChatOrderHandler extends OrderBaseHandler<Object,Object> {

    @Override
    public Object execute(Object param) {
        System.out.println("微信订单");
        return null;
    }

}
