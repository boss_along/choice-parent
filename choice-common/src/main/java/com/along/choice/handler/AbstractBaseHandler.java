package com.along.choice.handler;

import cn.hutool.core.annotation.AnnotationUtil;
import com.along.choice.handler.annotation.HandlerType;
import com.along.choice.handler.constant.HandlerTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;

@Setter
@Getter
public abstract class AbstractBaseHandler<REQ,RESP> implements IBaseHandler<REQ,RESP>,InitializingBean {

    private HandlerContext handlerContext;

    @Override
    public void afterPropertiesSet() throws Exception {
        Class<? extends AbstractBaseHandler> aClass = this.getClass();
        HandlerType annotation = AnnotationUtil.getAnnotation(aClass, HandlerType.class);
        HandlerTypeEnum value = annotation.value();
        handlerContext.putTypeEnumInstance(value,aClass);
    }
}
