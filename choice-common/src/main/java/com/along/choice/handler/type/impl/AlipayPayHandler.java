package com.along.choice.handler.type.impl;

import com.along.choice.handler.HandlerContext;
import com.along.choice.handler.annotation.HandlerType;
import com.along.choice.handler.constant.HandlerTypeEnum;
import com.along.choice.handler.type.PayBaseHandler;

import java.util.Map;

/**
 * @author Thinkpad
 * @Title: AlipayPayHandler
 * @ProjectName blog
 * @Description: TODO
 * @date 2019/6/1819:44
 * @Version: 1.0
 */
@HandlerType(HandlerTypeEnum.ALIPAY_PAY)
public class AlipayPayHandler extends PayBaseHandler<Object,Object> {

    @Override
    public Object execute(Object param) {
        System.out.println("支付宝支付");
        return null;
    }

}
