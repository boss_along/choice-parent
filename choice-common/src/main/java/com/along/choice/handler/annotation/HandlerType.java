package com.along.choice.handler.annotation;

import com.along.choice.handler.constant.HandlerTypeEnum;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface HandlerType {

    HandlerTypeEnum value();

}
