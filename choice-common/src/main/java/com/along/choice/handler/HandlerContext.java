package com.along.choice.handler;

import com.along.choice.handler.constant.HandlerTypeEnum;
import com.along.choice.utils.AppUtil;
import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Objects;

/**
 * @author Thinkpad
 * @Title: HandlerContext
 * @ProjectName blog
 * @Description: TODO
 * @date 2019/5/3019:42
 * @Version: 1.0
 */
public class HandlerContext {

    private  Map<HandlerTypeEnum,Class> handlerMap = Maps.newHashMap();

    public IBaseHandler getInstance(HandlerTypeEnum type){
        Class clazz = handlerMap.get(type);
        if(Objects.isNull(clazz)){
            throw new IllegalArgumentException("not found one initialized by handlerType");
        }
        return (IBaseHandler) AppUtil.getBean(clazz);
    }

    public void putTypeEnumInstance(HandlerTypeEnum type,Class clazz){
        handlerMap.put(type,clazz);
    }
}
