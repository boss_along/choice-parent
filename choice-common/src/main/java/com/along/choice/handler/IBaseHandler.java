package com.along.choice.handler;

/**
 * @author Thinkpad
 * @Title: BaseHandler
 * @ProjectName blog
 * @Description: TODO
 * @date 2019/5/3019:50
 * @Version: 1.0
 */
public interface IBaseHandler<REQ,RESP>{

    RESP execute(REQ request);

}
