package com.along.choice.config;

import com.along.choice.handler.HandlerContext;
import com.along.choice.handler.type.impl.AlipayOrderHandler;
import com.along.choice.handler.type.impl.AlipayPayHandler;
import com.along.choice.handler.type.impl.WeChatOrderHandler;
import com.along.choice.handler.type.impl.WeChatPayHandler;
import com.along.choice.utils.AppUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestBeanConfiguration {

    @Bean
    public AppUtil getAppUtil(){
        return new AppUtil();
    }

    @Bean
    public HandlerContext getHandlerContext(){
        return new HandlerContext();
    }

    @Bean
    public AlipayOrderHandler getAlipayOrderHandler(){
        AlipayOrderHandler alipayOrderHandler = new AlipayOrderHandler();
        alipayOrderHandler.setHandlerContext(getHandlerContext());
        return alipayOrderHandler;
    }

    @Bean
    public AlipayPayHandler getAlipayPayHandler(){
        AlipayPayHandler alipayPayHandler = new AlipayPayHandler();
        alipayPayHandler.setHandlerContext(getHandlerContext());
        return alipayPayHandler;
    }

    @Bean
    public WeChatOrderHandler getWeChatOrderHandler(){
        WeChatOrderHandler weChatOrderHandler = new WeChatOrderHandler();
        weChatOrderHandler.setHandlerContext(getHandlerContext());
        return weChatOrderHandler;
    }

    @Bean
    public WeChatPayHandler getWeChatPayHandler(){
        WeChatPayHandler weChatPayHandler = new WeChatPayHandler();
        weChatPayHandler.setHandlerContext(getHandlerContext());
        return weChatPayHandler;
    }
}
